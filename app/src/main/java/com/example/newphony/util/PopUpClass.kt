package com.example.newphony.util

import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.example.newphony.R
import com.example.newphony.BluetoothActivity
import com.example.newphony.PlayerActivity
import kotlinx.android.synthetic.main.popup_lay.view.*


class PopUpClass {
    fun showPopupWindow(view: View) {
        //Create a View object yourself through inflater
        val popupView: View = LayoutInflater.from(view.context).inflate(R.layout.popup_lay, null)
        //Specify the length and width through constants
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        //Make Inactive Items Outside Of PopupWindow
        val focusable = true
        //Create a window with our parameters
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 400, view.y.toInt()-140 )
        //Handler for clicking on the inactive zone of the window
        popupView.setOnTouchListener { v, event -> //Close the window when clicked
            popupWindow.dismiss()
            true
        }
        popupView.btnBlue.setOnClickListener {
            it.context.startActivity(Intent(it.context, BluetoothActivity::class.java))
            popupWindow.dismiss()
        }
        popupView.playNow.setOnClickListener {
            it.context.startActivity(Intent(it.context, PlayerActivity::class.java))
            popupWindow.dismiss()
        }
    }
}