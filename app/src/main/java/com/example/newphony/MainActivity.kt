package com.example.newphony

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.newphony.Intro.IntroScreenActivity

class MainActivity : AppCompatActivity() {
    private val handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        handler.postDelayed(runnable, 1500)
    }

    private val runnable = Runnable {
        startActivity(Intent(this@MainActivity, IntroScreenActivity::class.java))
        finish()
    }
}