package com.example.newphony

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.newphony.util.changeStatusBarColor
import kotlinx.android.synthetic.main.activity_bluetooth.*

class BluetoothActivity : AppCompatActivity() {
    var i = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)
        changeStatusBarColor(Color.BLACK)
        imgMain.setOnClickListener {
            when (i) {
                1 -> {
                    imgMain.setImageResource(R.drawable.scr_sensor)
                    icIcon.setImageResource(R.drawable.ic_sensor)
//                    imgMain.setBackgroundResource(R.drawable.scr_sensor)
                    i++
                }
                2 -> {
                    imgMain.setImageResource(R.drawable.scr_brain)
                    icIcon.setImageResource(R.drawable.ic_map)
//                    imgMain.setBackgroundResource(R.drawable.scr_brain)
                }
            }
        }
        icBack.setOnClickListener { onBackPressed() }
    }
}