package com.example.newphony

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.newphony.util.changeStatusBarColor
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        changeStatusBarColor(Color.parseColor("#423F91"))
        icBack.setOnClickListener { onBackPressed() }
    }
}