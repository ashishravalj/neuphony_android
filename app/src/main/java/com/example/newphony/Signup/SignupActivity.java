package com.example.newphony.Signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.newphony.Login.LoginActivity;
import com.example.newphony.R;
import com.example.newphony.Utils.Utils;
import com.example.newphony.databinding.ActivityLoginBinding;
import com.example.newphony.databinding.ActivitySignupBinding;

public class SignupActivity extends AppCompatActivity {


    ActivitySignupBinding signupBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signupBinding = DataBindingUtil.setContentView(SignupActivity.this,R.layout.activity_signup);
        Listener();

    }

    private void Listener() {
        signupBinding.txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
