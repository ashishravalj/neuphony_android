package com.example.newphony.Utils

import android.app.Activity
import android.content.Context
import android.content.Intent


/*Created by Kalpes Rajput*/

object Utils {
    fun startActivity(context: Context, class1: Class<*>?) {
        val intent = Intent()
        intent.setClass(context, class1!!)
        (context as Activity).startActivity(intent)
    }

}
