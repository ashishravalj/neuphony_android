package com.example.newphony.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newphony.R
import com.example.newphony.data.SubItem

class SubItemAdapter(private val subItemList: ArrayList<SubItem>) :
    RecyclerView.Adapter<ViewHolderCommon>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCommon {
        return ViewHolderCommon(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home_sub, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return subItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolderCommon, position: Int) {

    }
}