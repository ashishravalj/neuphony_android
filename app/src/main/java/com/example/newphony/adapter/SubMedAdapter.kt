package com.example.newphony.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newphony.R
import com.example.newphony.ui.MediationFragment

class SubMedAdapter(val list: ArrayList<String>, private val mediationFragment: MediationFragment) :
    RecyclerView.Adapter<ViewHolderCommon>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCommon {
        return ViewHolderCommon(
            LayoutInflater.from(parent.context).inflate(R.layout.item_sub_med, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolderCommon, position: Int) {
        with(holder.itemView) {
            setOnClickListener { mediationFragment.loadTimeFragment1() }
        }
    }
}