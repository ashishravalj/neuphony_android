package com.example.newphony.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newphony.R
import com.example.newphony.data.HomeDataItem
import kotlinx.android.synthetic.main.item_home.view.*

class HomeDataAdapter(private val dataList: ArrayList<HomeDataItem>) :
    RecyclerView.Adapter<ViewHolderCommon>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCommon {
        return ViewHolderCommon(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolderCommon, position: Int) {
        with(holder.itemView) {
            recycleSubItem.layoutManager =
                LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
            recycleSubItem.adapter = SubItemAdapter(dataList[position].subItemList)
        }
    }
}