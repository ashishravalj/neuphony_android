package com.example.newphony.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newphony.R
import com.example.newphony.data.MedItem
import com.example.newphony.ui.MediationFragment
import kotlinx.android.synthetic.main.item_med.view.*

class MedAdapter(val list: ArrayList<MedItem>,private val mediationFragment:MediationFragment) : RecyclerView.Adapter<ViewHolderCommon>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCommon {
        return ViewHolderCommon(
            LayoutInflater.from(parent.context).inflate(R.layout.item_med, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolderCommon, position: Int) {
        with(holder.itemView) {
            cardMain.setBackgroundColor(list[position].color)
            val lp = cardMain.layoutParams
            lp.height = list[position].height
            cardMain.layoutParams = lp
            setOnClickListener { mediationFragment.loadTimeFragment() }
        }
    }

}