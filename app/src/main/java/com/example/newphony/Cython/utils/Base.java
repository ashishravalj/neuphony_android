package com.example.newphony.Cython.utils;

public interface Base {
    void loadElements();
    void setListeners();
}
