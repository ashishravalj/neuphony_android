package com.example.newphony.Cython.utils;

public class Constants {

    // Constants for receiver 1000+
    public static final String USE_DEVICE_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    public static final String USB_DEVICE_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";


    public class Permissions{
        public static final String PERMS_USB = "com.bhavya.USB_PERMISSION";
        public static final int PERMS_EXTRN_STORAGE = 1001;
    }

}


