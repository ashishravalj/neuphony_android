package com.example.newphony.Cython;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.nio.charset.StandardCharsets;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.newphony.Cython.utils.Base;
import com.example.newphony.Cython.utils.Constants;
import com.example.newphony.Cython.utils.FormatDataForFile;
import com.example.newphony.Cython.utils.OpenBCIDataConversion;
import com.example.newphony.Cython.utils.SaveBytesToFile;
import com.example.newphony.R;
import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StartMeditationActivity extends AppCompatActivity implements Base, View.OnClickListener {

    private ImageView imageView;

    private Button btnSaveLog;
    private Button btnCheckConn;
    private Button btnStartStopMed;

    private ScrollView logScrollEEG;
    private TextView tvLog;

    private CharSequence[] channels = {
            "Channel 1", "Channel 2", "Channel 3",
            "Channel 4", "Channel 5", "Channel 6",
            "Channel 7", "Channel 8"
    };

    private boolean[] checkedChannels = {
            true, true, true,
            true, true, true,
            true, true
    };

    private boolean isProecessStarted = false;

    private UsbDevice usbDevice;
    private UsbDeviceConnection usbDeviceConnection;
    private UsbSerialDevice serial;
    private UsbManager usbManager;

    private BroadcastReceiver receiver;


    private StringBuilder builder;
    private String arrow = "  -->  ";

    private File directory = new File(
            Environment.getExternalStorageDirectory(), "/Newphony");

    // Filename for each session
    private String mFilenamePrefix = "CytonData_";
    private String mExtention = ".csv";
    private String mFilenameSuffix = "";
    private String mFilename = "Cyton.csv"; // Default Filename

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_meditation);

        loadElements();
        setListeners();
    }

    // loading and initiating the primary elements
    @Override
    public void loadElements() {

        toolbar = findViewById(R.id.toolbar);



        imageView = findViewById(R.id.imageView);



        logScrollEEG = findViewById(R.id.start_med_log_scroll_eeg);
        tvLog = findViewById(R.id.start_med_log);

        btnSaveLog = findViewById(R.id.start_med_save_log);
        btnCheckConn = findViewById(R.id.start_med_check_conn);
        btnStartStopMed = findViewById(R.id.start_med_start_stop_btn);

        handleButton();

        builder = new StringBuilder();

        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                switch (action != null ? action : "empty") {
                    case Constants.USE_DEVICE_ATTACHED:
                        buildLogger(String.valueOf(timestamp), "OTG Attached");
                        usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        int deviceId = usbDevice.getDeviceId();
                        int vendorId = usbDevice.getVendorId();
                        int productId = usbDevice.getProductId();
                        String devManufacturer = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            devManufacturer = usbDevice.getManufacturerName();
                        }

                        buildLogger("deviceId", String.valueOf(deviceId));
                        buildLogger("vendorId", String.valueOf(vendorId));
                        buildLogger("productId", String.valueOf(productId));
                        buildLogger("manufacturer", devManufacturer);

                        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                getApplicationContext(),
                                0,
                                new Intent(Constants.Permissions.PERMS_USB),
                                0
                        );

                        usbManager.requestPermission(usbDevice, pendingIntent);
                        break;
                    case Constants.USB_DEVICE_DETACHED:
                        stopDev();
                        buildLogger(String.valueOf(timestamp), "OTD Detached");
                        break;

                    case Constants.Permissions.PERMS_USB:
                        boolean perms = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);

                        if (perms) {
                            buildLogger("Permission", "Granted");

                            usbDeviceConnection = usbManager.openDevice(usbDevice);
                            serial = UsbSerialDevice.createUsbSerialDevice(usbDevice, usbDeviceConnection);

                            if (serial != null) {
                                buildLogger("Serial", "OK");
                                if (serial.open()) {
                                    buildLogger("Status", "Opening serial interface");
                                    serial.setBaudRate(115200);
                                    serial.setDataBits(UsbSerialInterface.DATA_BITS_8);
                                    serial.setStopBits(UsbSerialInterface.STOP_BITS_1);
                                    serial.setParity(UsbSerialInterface.PARITY_NONE);
                                    serial.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);

                                    String strChannelData = "x1060110Xx2160110Xx3160110Xx4160110Xx5160110Xx6160110Xx7160110Xx8160110X";
                                    byte[] channelData = new byte[0];
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                                        channelData = strChannelData.getBytes(StandardCharsets.UTF_8);
                                    }
                                    sendCmd(channelData);
                                }
                            }

                            buildLogger("--------------------------------------");
                            buildLogger("--------------------------------------\n");
                        } else {
                            Toast.makeText(StartMeditationActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

                updateLogger();
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.USE_DEVICE_ATTACHED);
        filter.addAction(Constants.USB_DEVICE_DETACHED);
        filter.addAction(Constants.Permissions.PERMS_USB);
        registerReceiver(receiver, filter);
    }

    // setting up listeners for each elements
    @Override
    public void setListeners() {
        imageView.setOnClickListener(this);
        btnSaveLog.setOnClickListener(this);
        btnCheckConn.setOnClickListener(this);
        btnStartStopMed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Set Channels");

                builder.setMultiChoiceItems(channels, checkedChannels, (dialogInterface, i, b) -> {

                });

                builder.setPositiveButton("SET", (dialogInterface, i) -> {

                });
                builder.setNegativeButton("Discard", (dialogInterface, i) -> {

                });
                builder.create().show();
                break;
            case R.id.start_med_start_stop_btn:
                handleButton();
                break;
            case R.id.start_med_save_log:
                String str = tvLog.getText().toString();
                if (str.length() > 0) {
                    handleSaveData();
                } else {
                    Toast.makeText(this, "Please attach the device", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.start_med_check_conn:
                String checkConnectionCmd = "?v";
                byte[] bytes = new byte[0];
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    bytes = checkConnectionCmd.getBytes(StandardCharsets.UTF_8);
                }
                sendCmd(bytes);

                if(serial!=null)
                {
                    serial.read(data -> {
                        buildLogger(new String(data));
                    });



                }else
                {
                    Toast.makeText(getApplicationContext(), "Error Occured", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    byte[][] dataForAsyncTask;
    // start / stop button for meditation
    private void handleButton() {
        if (isProecessStarted) {
            String strCmd = "b";
            byte[] bytes = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                bytes = strCmd.getBytes(StandardCharsets.UTF_8);
            }
            sendCmd(bytes);
            btnStartStopMed.setText("Stop Meditation");
            btnStartStopMed.setBackgroundResource(R.drawable.btnstop);
            isProecessStarted = false;

            try{
                serial.read(data -> {
                    byte[] packetData = Arrays.copyOfRange(data, 1, data.length);
                    int packetNumber = data[0] & 0xFF;
                   // byte[][] dataForAsyncTask = {mFilename.getBytes(), data};
                   // Toast.makeText(StartMeditationActivity.this,dataForAsyncTask.length+"",Toast.LENGTH_LONG).show();
                    /*if(dataForAsyncTask.length > 0){
                        SaveToExcel();
                    }*/
                    buildLogger("Packet Number : " + packetNumber, FormatDataForFile.convertBytesToHex(packetData));
                });
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),"Error Occured", Toast.LENGTH_SHORT).show();
            }

        } else {
            String strCmd = "s";
            byte[] bytes = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                bytes = strCmd.getBytes(StandardCharsets.UTF_8);
            }
            sendCmd(bytes);
            btnStartStopMed.setText("Start Meditation");
            btnStartStopMed.setBackgroundResource(R.drawable.btnstart);
            isProecessStarted = true;

            try{
                serial.read(data -> {
                   //Toast.makeText(StartMeditationActivity.this,"arduinoData.length+",Toast.LENGTH_LONG).show();

                    buildLogger(new String(data));
                    try{
                        byte[] packetData = Arrays.copyOfRange(data, 1, data.length);
                        int packetNumber = data[0] & 0xFF;
                        buildLogger(String.valueOf(packetNumber), FormatDataForFile.convertBytesToHex(packetData));
                         dataForAsyncTask = new byte[][]{mFilename.getBytes(), data};
                         this.dataForAsyncTask = dataForAsyncTask;
                        //new SaveBytesToFile().execute(dataForAsyncTask);
                        //SaveToExcel(dataForAsyncTask);
                    }catch (Exception e){e.printStackTrace();}



                });
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),"Error Occured", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void SaveToExcel(byte[][] arduinoData){

        try {
           // Toast.makeText(StartMeditationActivity.this,arduinoData.length+"",Toast.LENGTH_LONG).show();
            // Get file name for current session
           // String filename = Arrays.toString(arduinoData[0]) + ".csv";
           String filename = "DataLog.csv";
            // Store data from RFduino
            byte[] packet = arduinoData[1];
            // Store packet data
            // The first byte is the sample counter
            byte sampleCounter = packet[0]; // 1 sample counter
            // Next we have 6 data values 3 bytes each
            byte[] packetDataChannel1 = Arrays.copyOfRange(packet, 1, 4);
            byte[] packetDataChannel2 = Arrays.copyOfRange(packet, 4, 7);
            byte[] packetDataChannel3 = Arrays.copyOfRange(packet, 7, 10);
            byte[] packetDataChannel4 = Arrays.copyOfRange(packet, 10, 13);
            byte[] packetDataChannel5 = Arrays.copyOfRange(packet, 13, 16);
            byte[] packetDataChannel6 = Arrays.copyOfRange(packet, 16, 19);
            // The last byte is an Auxiliary byte
            byte packetDataAux = packet[19];

            // Data Conversion for File Writing
            // Get the sample number
            int sNumber = sampleCounter & 0xFF;
            String sampleNumberString = Integer.valueOf(sNumber).toString();

            // Get Channel data in micro Volts
            float[] channel = new float[6];
            channel[0] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel1);
            channel[1] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel2);
            channel[2] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel3);
            channel[3] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel4);
            channel[4] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel5);
            channel[5] = OpenBCIDataConversion
                    .convertByteToMicroVolts(packetDataChannel6);

            // Get Auxiliary byte
            int auxNumber = packetDataAux & 0xFF;
            String auxNumberString = Integer.valueOf(auxNumber).toString();

            // Create the file for the current session
            File directory = new File(Environment.getExternalStorageDirectory(),
                    "/Vritti");
            File file = new File(directory.getAbsolutePath(), filename);
            DataOutputStream dos;
            directory.mkdirs();

            try {
                dos = new DataOutputStream(new FileOutputStream(file.getPath(),
                        true));
                dos.writeChars(sampleNumberString);
                dos.writeChars(", ");
                dos.writeChars(FormatDataForFile.convertFloatArrayToString(channel));
                dos.writeChars(auxNumberString);
                dos.write(System.getProperty("line.separator").getBytes());
                dos.close();
            } catch (FileNotFoundException e) {
                Log.e("Save to File AsyncTask", "Error finding file");
                // Crashlytics.log(Log.ERROR, "SaveToFileAsync", e.getMessage());
            } catch (IOException e) {
                Log.e("Save to File AsyncTask", "Error saving data");
                // Crashlytics.log(Log.ERROR, "IOExcAsyc", e.getMessage());
            }
        }catch (Exception e){e.printStackTrace();}


    }


    // starting and otg devices manually
    private void startDev() {
        if (usbDevice == null) {
            HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();

            if (!usbDevices.isEmpty()) {
                for (Map.Entry entry : usbDevices.entrySet()) {

                    usbDevice = (UsbDevice) entry.getValue();
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    buildLogger(String.valueOf(timestamp), "OTG Attached");
                    int deviceId = usbDevice.getDeviceId();
                    int vendorId = usbDevice.getVendorId();
                    int productId = usbDevice.getProductId();
                    String devManufacturer = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        devManufacturer = usbDevice.getManufacturerName();
                    }

                    buildLogger("deviceId", String.valueOf(deviceId));
                    buildLogger("vendorId", String.valueOf(vendorId));
                    buildLogger("productId", String.valueOf(productId));
                    buildLogger("manufacturer", devManufacturer);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            getApplicationContext(),
                            0,
                            new Intent(Constants.Permissions.PERMS_USB),
                            0
                    );

                    usbManager.requestPermission(usbDevice, pendingIntent);
                }
            } else {
                buildLogger("Error", "Device not connected");
                updateLogger();
            }
        } else {
            Toast.makeText(this, "Device already connected", Toast.LENGTH_SHORT).show();
        }
    }

    // stopping the serial connection when it's not needed.
    private void stopDev() {
        if (serial != null) {
            serial.close();
        }
    }

    // sending specific commands
    private void sendCmd(byte[] cmd) {
        if (serial != null) {
            serial.write(cmd);
            buildLogger(new String(cmd), "sent");
        }
        updateLogger();
    }

    // building logger with key only
    private void buildLogger(String key) {
        builder.append(key).append("\n");
    }

    // building logger with key,val pair
    private void buildLogger(String key, String val) {
        builder.append(key).append(arrow).append(val).append("\n");
    }

    // invoke permission for writing on external device
    private void handleSaveData() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            saveData();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.Permissions.PERMS_EXTRN_STORAGE);
        }
    }

    // update logger to scroll down pos.
    private void updateLogger() {
        tvLog.setText(builder);

        logScrollEEG.post(() -> {
            logScrollEEG.fullScroll(View.FOCUS_DOWN);
        });
    }

    // Method for saving log data to external storage
    private void saveData() {

       /// new SaveBytesToFile().execute(packetData2);
       // new SaveBytesToFile().execute(dataForAsyncTask);
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/Vritti");
        dir.mkdirs();
        File file = new File(dir, "LogData.txt");

        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            pw.write(builder.toString());
            pw.flush();
            pw.close();
            f.close();

           // new SaveBytesToFile().execute(dataForAsyncTask);
           // SaveToExcel(dataForAsyncTask);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            buildLogger("Error", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            buildLogger("Error", e.getMessage());
        }
    }

    // Requesting permission for accessing OTG devices.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.Permissions.PERMS_EXTRN_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveData();
            } else {
                Toast.makeText(this, "Permission denied by User", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
