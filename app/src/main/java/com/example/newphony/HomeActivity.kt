package com.example.newphony

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.newphony.util.transformers.AntiClockSpinTransformation
import com.example.newphony.util.transformers.ZoomInTransformer
import com.example.newphony.util.transformers.ZoomOutPageTransformer
import kotlinx.android.synthetic.main.activity_home.*
//https://www.figma.com/file/PSdogwgK1IoagzXrNo3d2y/neuphony?node-id=268%3A5101

class HomeActivity : AppCompatActivity() {
    private lateinit var dots: Array<TextView?>
    private lateinit var layouts: IntArray
    private lateinit var mAdapter: ViewsSliderAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        init()
    }

    private fun init() {
        layouts = intArrayOf(
            R.layout.slide_one,
            R.layout.slide_two,
            R.layout.slide_three
        )
        mAdapter = ViewsSliderAdapter()
        view_pager.adapter = mAdapter
        view_pager.registerOnPageChangeCallback(pageChangeCallback)
        view_pager.setPageTransformer(ZoomInTransformer())
        addBottomDots(0)
    }
    var pageChangeCallback: OnPageChangeCallback = object : OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            addBottomDots(position)
        }
    }
    private fun addBottomDots(currentPage: Int) {
        dots = arrayOfNulls<TextView?>(layouts.size)
        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)
        layoutDots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]?.text   = Html.fromHtml("&#8226;")
            dots[i]?.textSize  = 80F
            dots[i]?.setTextColor(colorsInactive[currentPage])
            layoutDots.addView(dots[i])
        }
        if (dots.isNotEmpty()) dots[currentPage]?.setTextColor(colorsActive[currentPage])
    }
   inner class ViewsSliderAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): RecyclerView.ViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(viewType, parent, false)
            return SliderViewHolder(view)
        }

        override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder,
            position: Int
        ) {
        }

        override fun getItemViewType(position: Int): Int {
            return layouts[position]
        }

        override fun getItemCount(): Int {
            return layouts.size
        }

        inner class SliderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        }
    }
}