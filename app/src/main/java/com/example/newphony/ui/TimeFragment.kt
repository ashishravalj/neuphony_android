package com.example.newphony.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newphony.R
import com.example.newphony.adapter.SubMedAdapter
import com.example.newphony.util.changeStatusBarColor
import kotlinx.android.synthetic.main.fragment_time.*

class TimeFragment(private val mediationFragment: MediationFragment) : Fragment(R.layout.fragment_time) {
    private lateinit var subMedAdapter: SubMedAdapter
    private val list = ArrayList<String>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).changeStatusBarColor(resources.getColor(R.color.red_new))
        init()
    }

    private fun init() {
        for (i in 0..20) {
            list.add("")
        }
        subMedAdapter = SubMedAdapter(list,mediationFragment)
        recycleViewTime.layoutManager = LinearLayoutManager(requireContext())
        recycleViewTime.adapter = subMedAdapter
        icBack.setOnClickListener { mediationFragment.loadFragment() }
    }
}