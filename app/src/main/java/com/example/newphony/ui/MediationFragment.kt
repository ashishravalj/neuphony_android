package com.example.newphony.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.newphony.R

class MediationFragment : Fragment(R.layout.fragment_mediation_main) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        loadFragment()
    }

     fun loadFragment() {
        childFragmentManager.beginTransaction().replace(R.id.container, MeditationChildFragment(this)).commit()
    }
     fun loadTimeFragment() {
        childFragmentManager.beginTransaction().replace(R.id.container, TimeFragment(this)).commit()
    }
     fun loadTimeFragment1() {
        childFragmentManager.beginTransaction().replace(R.id.container, MedChild2Fragment(this)).commit()
    }
}