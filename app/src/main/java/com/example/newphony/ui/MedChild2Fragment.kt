package com.example.newphony.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.newphony.BluetoothActivity
import com.example.newphony.PlayerActivity
import com.example.newphony.R
import com.example.newphony.util.PopUpClass
import kotlinx.android.synthetic.main.fragment_mediation_two.*
import kotlinx.android.synthetic.main.popup_lay.view.*

class MedChild2Fragment(private val mediationFragment: MediationFragment) :
        Fragment(R.layout.fragment_mediation_two) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        icBack.setOnClickListener { mediationFragment.loadTimeFragment() }
        corLay.setOnClickListener { popView.visibility = View.GONE }
        btnPlay.setOnClickListener {
            if (popView.visibility == View.GONE) {
                popView.visibility = View.VISIBLE
            } else {
                popView.visibility = View.GONE
            }
        }
        btnBlue.setOnClickListener {
            popView.visibility = View.GONE
            it.context.startActivity(Intent(it.context, BluetoothActivity::class.java))
        }
        playNow.setOnClickListener {
            popView.visibility = View.GONE
            it.context.startActivity(Intent(it.context, PlayerActivity::class.java))
        }
    }
}