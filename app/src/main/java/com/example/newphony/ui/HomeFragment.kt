package com.example.newphony.ui

import android.R.attr.data
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newphony.Cython.StartMeditationActivity
import com.example.newphony.Login.LoginActivity
import com.example.newphony.R
import com.example.newphony.Utils.Utils
import com.example.newphony.adapter.HomeDataAdapter
import com.example.newphony.data.HomeDataItem
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment(R.layout.fragment_home) {
    private val dataList = ArrayList<HomeDataItem>()
    private lateinit var homeDataAdapter: HomeDataAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        init()
    }

    private fun init() {
        for (i in 0..4){
            dataList.add(HomeDataItem())
        }
        homeDataAdapter = HomeDataAdapter(dataList)
        recycleViewMain.layoutManager = LinearLayoutManager(requireContext())
        recycleViewMain.adapter = homeDataAdapter
        appBar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    txtName1.visibility = View.VISIBLE
                    proImg1.visibility = View.VISIBLE
                    txtName.visibility = View.INVISIBLE
                    proImg.visibility = View.INVISIBLE
                    toolbar.visibility = View.VISIBLE
                } else if (isShow) {
                    isShow = false
                    txtName1.visibility = View.INVISIBLE
                    proImg1.visibility = View.INVISIBLE
                    txtName.visibility = View.VISIBLE
                    proImg.visibility = View.VISIBLE
                    toolbar.visibility = View.INVISIBLE
                }
            }
        })

        proImg.setOnClickListener {

            activity?.let { it1 -> Utils.startActivity(it1, LoginActivity::class.java) }
            //activity?.let { it1 -> Utils.startActivity(it1, StartMeditationActivity::class.java) }
        }
    }

}