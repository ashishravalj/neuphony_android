package com.example.newphony.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.invalidateOptionsMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.newphony.R
import com.example.newphony.adapter.MedAdapter
import com.example.newphony.data.MedItem
import com.example.newphony.util.changeStatusBarColor
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
//import kotlinx.android.synthetic.main.fragment_mediation.*
import kotlinx.android.synthetic.main.frag_med.*
import kotlin.math.abs


class MeditationChildFragment(private val mediationFragment: MediationFragment) :
        Fragment(R.layout.frag_med) {
    private lateinit var medAdapter: MedAdapter
    private val medList = ArrayList<MedItem>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).changeStatusBarColor(resources.getColor(R.color.med_status_bar))
        init()
    }

    private fun init() {
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_1).toInt(),
                        Color.parseColor("#E0F4675D")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_2).toInt(),
                        Color.parseColor("#C24BC6D8")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_2).toInt(),
                        Color.parseColor("#FFD1D7")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_1).toInt(),
                        Color.parseColor("#78424E95")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_2).toInt(),
                        Color.parseColor("#825D3382")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_1).toInt(),
                        Color.parseColor("#8C29ABE2")
                )
        )
        medList.add(
                MedItem(
                        resources.getDimension(R.dimen.height_2).toInt(),
                        Color.parseColor("#D9F9D340")
                )
        )
        medAdapter = MedAdapter(medList, mediationFragment)
        recycleViewMed.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        recycleViewMed.adapter = medAdapter
        icBack.setOnClickListener { requireActivity().onBackPressed() }
        appBar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    txtCenter1.visibility = View.VISIBLE
                    icSearch.visibility = View.VISIBLE
                    txtCenter.visibility = View.INVISIBLE
                } else if (isShow) {
                    isShow = false
                    txtCenter1.visibility = View.INVISIBLE
                    icSearch.visibility = View.INVISIBLE
                    txtCenter.visibility = View.VISIBLE
                }
            }
        })
    }
}