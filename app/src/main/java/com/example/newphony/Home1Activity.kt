package com.example.newphony

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.newphony.util.changeStatusBarColor
import kotlinx.android.synthetic.main.activity_home_1.*
import kotlinx.android.synthetic.main.item_sub_med.*


class Home1Activity : AppCompatActivity(), NavController.OnDestinationChangedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_1)
        init()
    }

    private fun init() {
        val navController = findNavController(R.id.nav_host_fragment)
        bottomNavigationView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener(this)
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.navigation_home -> {
                botLay.setBackgroundResource(R.drawable.back_navigation_view_1)
                changeStatusBarColor(Color.BLACK)
            }
            R.id.navigation_meditation -> {
                botLay.setBackgroundResource(R.drawable.back_navigation_view)
                changeStatusBarColor(resources.getColor(R.color.med_status_bar))
            }
        }
    }
}