package com.example.newphony.Setting;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.newphony.R;
import com.example.newphony.databinding.ActivitySettingAfterLoginBinding;

public class SettingAfterLoginActivity extends AppCompatActivity {


    ActivitySettingAfterLoginBinding settingAfterLoginBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingAfterLoginBinding = DataBindingUtil.setContentView(SettingAfterLoginActivity.this,R.layout.activity_setting_after_login);
        Listener();

    }

    private void Listener() {

    }
}
