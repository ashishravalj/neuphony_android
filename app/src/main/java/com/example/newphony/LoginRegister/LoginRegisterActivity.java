package com.example.newphony.LoginRegister;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.newphony.Login.LoginActivity;
import com.example.newphony.R;
import com.example.newphony.Setting.SettingAfterLoginActivity;
import com.example.newphony.databinding.ActivityLoginRegisterBinding;
import com.example.newphony.databinding.ActivityLoginRegisterBindingImpl;

public class LoginRegisterActivity extends AppCompatActivity {


    ActivityLoginRegisterBinding loginRegisterBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginRegisterBinding = DataBindingUtil.setContentView(LoginRegisterActivity.this,R.layout.activity_login_register);
        Listener();

    }

    private void Listener() {

        loginRegisterBinding.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginRegisterActivity.this, SettingAfterLoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
