package com.example.newphony.data

class HomeDataItem {
    val subItemList = ArrayList<SubItem>()

    init {
        subItemList.add(SubItem())
        subItemList.add(SubItem())
        subItemList.add(SubItem())
        subItemList.add(SubItem())
    }
}